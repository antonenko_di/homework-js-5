function createNewUser() {
    const newUser = {};

    newUser.firstName = prompt("Введіть Ваше ім'я:");
    newUser.lastName = prompt("Введіть Ваше прізвище:");

    newUser.getLogin = function() {
        const firstLetter = this.firstName.charAt(0).toLowerCase();
        return firstLetter + this.lastName.toLowerCase();
    };

    return newUser;
}

const user = createNewUser();
const login = user.getLogin();
console.log(login);

